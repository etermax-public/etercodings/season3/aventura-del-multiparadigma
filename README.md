<!-----
NEW: Check the "Suppress top comment" option to remove this info from the output.

Conversion time: 0.348 seconds.


Using this Markdown file:

1. Paste this output into your source file.
2. See the notes and action items below regarding this conversion run.
3. Check the rendered output (headings, lists, code blocks, tables) for proper
   formatting and use a linkchecker before you publish this page.

Conversion notes:

* Docs to Markdown version 1.0β30
* Thu Jul 29 2021 13:21:19 GMT-0700 (PDT)
* Source doc: La aventura del Multiverso
----->


**_La aventura del Multiverso_**

Hace poco, nuestro amigo el Dr. Estaban Extraño nos contó que no estamos solos en el `Universo`! Es más, nos explicó que existen múltiples universos!!! A esto lo llamo `Multiverso`.

El problema es que algunos universos quieren conquistar al resto y dominar el multiverso!

Para evitar ser dominados, el Dr. Extraño se quiso preparar y, como todavía no encontró una gema del infinito del tiempo, nos pidió a nosotros un sistema para poder simular todos los posibles futuros y prepararnos para las batallas lo mejor posible.

Lo bueno es que nuestro querido Dr. descubrió que todos los `Universos` son bastante parecidos entre sí, y nos pasó una lista de las cosas más importantes a tener en cuenta.



* Cada `Universo` puede tener un ejército de `Guerreros`.
* Los `Guerreros` son “seres” que pueden atacar a otros.
* Hay 3 tipos de `Guerreros`: `Comunes`, `Poderosos` y `Héroes`
* Todos los `Guerreros` comienzan con 100 puntos de vida y tienen distintos niveles de ataque:
  * Un `Guerrero común` hace 10 puntos de daño
  * Un `Guerrero Poderoso` hace 30 puntos de daño
  * Un `Héroe` hace 90 puntos de daño

Y también descubrió las reglas que existen para estas batallas entre `Universos`.



* Si un `Universo` no tiene ejército, al no poder defenderse, es dominado inmediatamente.
* Si un `Universo` tiene ejército va a querer dominar al resto.
* Si hay un único `Universo` con ejército ese es el dominador de todo el Multiverso
* Si hay dos o más `Universos` con ejércitos alguno le declarará la guerra a otro y pelearán hasta que uno de los dos quede sin ejército.
* Cuando dos universos entran en guerra los Guerreros se atacan por turnos, primero el que inició la guerra ataca a un Guerrero del otro universo y luego el Universo atacado puede contraatacar, y así hasta que alguno de los dos pueda diezmar al ejército contrario.

Finalmente, el Dr. tiene la información de los Universos con ejércitos y conoce bien la situación militar del nuestro, con lo que podremos evaluar si seremos dominados o conquistadores!!!
